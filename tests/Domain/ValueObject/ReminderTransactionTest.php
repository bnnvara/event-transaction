<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Transaction\Domain\ValueObject;

use BNNVARA\Transaction\Domain\ValueObject\ReminderTransaction;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class ReminderTransactionTest extends TestCase
{
    /** @test */
    public function reminderTransactionCanBeCreated(): void
    {
        $transaction = new ReminderTransaction(
            'a payment id',
            'payment external id',
            10,
            new DateTimeImmutable('2022-01-01T00:00:00+00:00')
        );

        $this->assertSame('a payment id', $transaction->getPaymentId());
        $this->assertSame('payment external id', $transaction->getPaymentExternalId());
        $this->assertSame(10.0, $transaction->getAmount());
        $this->assertSame('2022-01-01T00:00:00+00:00', $transaction->getPaymentDate()->format('c'));
    }
}
