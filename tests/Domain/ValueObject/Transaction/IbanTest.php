<?php

namespace Tests\BNNVARA\Transaction\Domain\ValueObject\Transaction;

use BNNVARA\Transaction\Domain\ValueObject\Event\Iban;
use PHPUnit\Framework\TestCase;

class IbanTest extends TestCase
{
    /** @test */
    public function aValidIbanIsCreated()
    {
        $iban = new Iban('GB33BUKB20201555555555');
        $this->assertEquals('GB33BUKB20201555555555', (string)$iban);
    }
}
