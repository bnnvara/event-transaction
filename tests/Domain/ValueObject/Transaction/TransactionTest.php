<?php

namespace Tests\BNNVARA\Transaction\Domain\ValueObject;

use BNNVARA\Transaction\Domain\ValueObject\Event\Amount;
use BNNVARA\Transaction\Domain\ValueObject\Event\Email;
use BNNVARA\Transaction\Domain\ValueObject\Event\FirstName;
use BNNVARA\Transaction\Domain\ValueObject\Event\Iban;
use BNNVARA\Transaction\Domain\ValueObject\Event\Id;
use BNNVARA\Transaction\Domain\ValueObject\Event\NameAffix;
use BNNVARA\Transaction\Domain\ValueObject\Event\LastName;
use BNNVARA\Transaction\Domain\ValueObject\Event\PaymentTerm;
use BNNVARA\Transaction\Domain\ValueObject\Event\Status;
use BNNVARA\Transaction\Domain\ValueObject\Event\Transaction;
use BNNVARA\Transaction\Domain\ValueObject\Event\UTM;
use BNNVARA\Transaction\Domain\ValueObject\Event\UTMCollection;
use Tests\BNNVARA\Transaction\PrivatePropertyManipulator;
use PHPUnit\Framework\TestCase;
use ReflectionClass;

class TransactionTest extends TestCase
{
    use PrivatePropertyManipulator;

    /** @test */
    public function aValidTransactionCanBeCreated()
    {
        $transaction = new Transaction(
            new Id('id'),
            new Amount(500),
            new Status('PAID'),
            new Email('tjap@example.com'),
            new FirstName('Sjaakie'),
            new LastName('Chocoladefabriek'),
            new NameAffix('en de'),
            new Iban('GB33BUKB20201555555555'),
            new PaymentTerm('monthly'),
            new UTMCollection()
        );

        $this->assertEquals('id', $transaction->getId()->getValue());
        $this->assertEquals(500, $transaction->getAmount()->getValue());
        $this->assertEquals('PAID', $transaction->getStatus()->getValue());
        $this->assertEquals('tjap@example.com', $transaction->getEmail()->getValue());
        $this->assertEquals('Sjaakie', (string)$transaction->getFirstName());
        $this->assertEquals('Chocoladefabriek', (string)$transaction->getLastName());
        $this->assertEquals('en de', (string)$transaction->getNameAffix());
        $this->assertEquals('GB33BUKB20201555555555', (string)$transaction->getIban());
        $this->assertEquals('monthly', (string)$transaction->getPaymentTerm());
        $this->assertInstanceOf(UTMCollection::class, $transaction->getUtmCollection());
    }

    /** @test */
    public function aValidTransactionCanBeCreatedWithoutName()
    {
        $transaction = new Transaction(
            new Id('id'),
            new Amount(500),
            new Status('PAID'),
            new Email('tjap@example.com'),
            null,
            null,
            null,
            null,
            null,
            null
        );

        $this->assertEquals('id', $transaction->getId()->getValue());
        $this->assertEquals(500, $transaction->getAmount()->getValue());
        $this->assertEquals('PAID', $transaction->getStatus()->getValue());
        $this->assertEquals('tjap@example.com', $transaction->getEmail()->getValue());
        $this->assertNull($transaction->getLastName());
        $this->assertNull($transaction->getFirstName());
        $this->assertNull($transaction->getNameAffix());
    }

    /** @test */
    public function aValidTransactionCanBeCreatedWithAName()
    {
        $transaction = new Transaction(
            new Id('id'),
            new Amount(500),
            new Status('PAID'),
            new Email('tjap@example.com'),
            new FirstName('Sjaakie'),
            new LastName('Chocolade'),
            null,
            null,
            null,
            null
        );

        $this->assertEquals('id', $transaction->getId()->getValue());
        $this->assertEquals(500, $transaction->getAmount()->getValue());
        $this->assertEquals('PAID', $transaction->getStatus()->getValue());
        $this->assertEquals('tjap@example.com', $transaction->getEmail()->getValue());
        $this->assertEquals('Sjaakie', (string)$transaction->getFirstName());
        $this->assertEquals('Chocolade', (string)$transaction->getLastName());
        $this->assertNull($transaction->getNameAffix());
    }

    /** @test */
    public function aValidTransactionCanBeCreatedWithANameAffix()
    {
        $transaction = new Transaction(
            new Id('id'),
            new Amount(500),
            new Status('PAID'),
            new Email('tjap@example.com'),
            new FirstName('Sjaakie'),
            new LastName('Chocoladefabriek'),
            new NameAffix('en de'),
            null,
            null,
            null
        );

        $this->assertEquals('id', $transaction->getId()->getValue());
        $this->assertEquals(500, $transaction->getAmount()->getValue());
        $this->assertEquals('PAID', $transaction->getStatus()->getValue());
        $this->assertEquals('tjap@example.com', $transaction->getEmail()->getValue());
        $this->assertEquals('Sjaakie', (string)$transaction->getFirstName());
        $this->assertEquals('Chocoladefabriek', (string)$transaction->getLastName());
        $this->assertEquals('en de', (string)$transaction->getNameAffix());
    }

    /** @test
     * By deserialising in adapter-payment, this same reflection behaviour is done,
     * the '=null' settings for optional properties is therefor required.
     */
    public function propertiesSetCorrectlyTest()
    {
        //set to force-add the optional and required properties on the right place.
        $requiredProperties = ['id', 'amount', 'status', 'email'];
        $optionalProperties = ['firstName', 'lastName', 'nameAffix', 'iban', 'paymentTerm', 'utmCollection'];

        $transaction = new ReflectionClass(Transaction::class);

        foreach ($transaction->getProperties() as $property) {
            $this->assertEquals(true,
                in_array($property->getName(), array_merge($requiredProperties, $optionalProperties)));
        }

        // Typed, optional properties must default to null by PHP7.4
        // Typed property [] must not be accessed before initialization
        $transactionReflection = $transaction->newInstanceWithoutConstructor();
        foreach ($optionalProperties as $property) {
            $this->assertNull($this->getByReflection($transactionReflection, $property));
        }
    }
}
