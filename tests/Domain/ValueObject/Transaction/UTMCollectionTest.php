<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Transaction\Domain\ValueObject\Transaction;

use BNNVARA\Transaction\Domain\ValueObject\Event\UTM;
use BNNVARA\Transaction\Domain\ValueObject\Event\UTMCollection;
use PHPUnit\Framework\TestCase;

class UTMCollectionTest extends TestCase
{
    /** @test */
    public function aCollectionCanBeCreated(): void
    {
        $utmCollection = new UTMCollection();
        $utmCollection->addUtm(
            new UTM(
                'term',
                'term'
            )
        );

        $this->assertCount(1, $utmCollection->getUtmCollection());
        $this->assertInstanceOf(UTM::class, $utmCollection->getUtmCollection()[0]);
    }
}