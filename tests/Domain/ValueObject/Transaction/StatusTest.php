<?php

namespace Tests\BNNVARA\Transaction\Domain\ValueObject;

use BNNVARA\Transaction\Domain\Exception\InvalidStatusException;
use BNNVARA\Transaction\Domain\ValueObject\Event\Status;
use PHPUnit\Framework\TestCase;

class StatusTest extends TestCase
{
    /** @test */
    public function aValidTransactionStatusIsAccepted()
    {
        $transactionStatus = new Status('PAID');
        $this->assertEquals('PAID', (string) $transactionStatus);
    }

    /** @test */
    public function anExceptionIsThrownWhenAnInvalidStatusIsCreated()
    {
        $this->expectException(InvalidStatusException::class);
        new Status('NOTPAID');
    }
}
