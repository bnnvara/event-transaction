<?php

namespace Tests\BNNVARA\Transaction\Domain\ValueObject;

use BNNVARA\Transaction\Domain\Exception\InvalidFirstNameException;
use BNNVARA\Transaction\Domain\ValueObject\Event\FirstName;
use PHPUnit\Framework\TestCase;

class FirstNameTest extends TestCase
{
    /** @test */
    public function aValidFirstNameIsCreated()
    {
        $firstName = new FirstName('sjaakie');
        $this->assertEquals('sjaakie', (string) $firstName);
    }

    /** @test */
    public function anInvalidFirstNameThrowsAnError()
    {
        $this->expectException(InvalidFirstNameException::class);
        new FirstName('sjaakie@dechocoladefabriek.nl');
    }

    /** @test */
    public function aTooLongFirstNameThrowsAnError()
    {
        $this->expectException(InvalidFirstNameException::class);
        new FirstName('van den Heuvel tot Beichlingen, gezegd Bartolotti Rijnders Beichlingen, gezegd Bartolotti Rijnders van den Heuvel tot Beichlingen, gezegd Bartolotti Rijnders');
    }

}
