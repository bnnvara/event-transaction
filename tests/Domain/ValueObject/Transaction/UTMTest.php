<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Transaction\Domain\ValueObject\Transaction;

use BNNVARA\Transaction\Domain\ValueObject\Event\UTM;
use PHPUnit\Framework\TestCase;

class UTMTest extends TestCase
{

    /** @test */
    public function anUTMCanBeCreated(): void
    {
        $utm = new UTM('term', 'term');

        $this->assertEquals('term', $utm->getName());
        $this->assertEquals('term', $utm->getValue());
    }
}