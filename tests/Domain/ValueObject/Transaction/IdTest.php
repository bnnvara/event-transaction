<?php

namespace Tests\BNNVARA\Transaction\Domain\ValueObject\Transaction;

use BNNVARA\Transaction\Domain\ValueObject\Event\Id;
use PHPUnit\Framework\TestCase;

class IdTest extends TestCase
{
    /** @test */
    public function aValidIdIsCreated()
    {
        $id = new Id('id');
        $this->assertEquals('id', (string) $id);
    }
}
