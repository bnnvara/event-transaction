<?php

namespace Tests\BNNVARA\Transaction\Domain\ValueObject;

use BNNVARA\Transaction\Domain\Exception\InvalidEmailException;
use BNNVARA\Transaction\Domain\ValueObject\Event\Email;
use PHPUnit\Framework\TestCase;

class EmailTest extends TestCase
{
    /** @test */
    public function aValidEmailIsCreated()
    {
        $email = new Email('sjaakie@example.com');
        $this->assertEquals('sjaakie@example.com', (string) $email);
    }


    /** @test */
    public function anExceptionIsThrownWhenAnInvalidEmailIsCreated()
    {
        $this->expectException(InvalidEmailException::class);
        $email = new Email('tjaappie');
    }
}
