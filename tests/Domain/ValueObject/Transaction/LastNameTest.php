<?php

namespace Tests\BNNVARA\Transaction\Domain\ValueObject;

use BNNVARA\Transaction\Domain\Exception\InvalidLastNameException;
use BNNVARA\Transaction\Domain\ValueObject\Event\LastName;
use PHPUnit\Framework\TestCase;

class LastNameTest extends TestCase
{
    /** @test */
    public function aValidLastNameIsCreated()
    {
        $lastName = new LastName('van den Heuvel tot Beichlingen, gezegd Bartolotti Rijnders');
        $this->assertEquals('van den Heuvel tot Beichlingen, gezegd Bartolotti Rijnders', (string) $lastName);
    }

    /** @test */
    public function anInvalidLastNameThrowsAnError()
    {
        $this->expectException(InvalidLastNameException::class);
        new LastName('sjaakie@dechocoladefabriek.nl');
    }

    /** @test */
    public function aTooLongLastNameThrowsAnError()
    {
        $this->expectException(InvalidLastNameException::class);
        new LastName('van den Heuvel tot Beichlingen, gezegd Bartolotti Rijnders Beichlingen, gezegd Bartolotti Rijnders van den Heuvel tot Beichlingen, gezegd Bartolotti Rijnders');
    }
}
