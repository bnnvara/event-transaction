<?php

namespace Tests\BNNVARA\Transaction\Domain\ValueObject;

use BNNVARA\Transaction\Domain\ValueObject\Event\Amount;
use PHPUnit\Framework\TestCase;

class AmountTest extends TestCase
{
    /** @test */
    public function aValidAmountIsCreated()
    {
        $amount = new Amount(200);
        $this->assertEquals(200, $amount->getValue());
    }

}
