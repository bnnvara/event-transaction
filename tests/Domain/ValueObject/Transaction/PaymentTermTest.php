<?php

namespace Tests\BNNVARA\Transaction\Domain\ValueObject\Transaction;

use BNNVARA\Transaction\Domain\Exception\InvalidPaymentTermTypeException;
use BNNVARA\Transaction\Domain\ValueObject\Event\PaymentTerm;
use PHPUnit\Framework\TestCase;

class PaymentTermTest extends TestCase
{
    /** @test */
    public function aValidPaymentTermIsCreated()
    {
        $interval = new PaymentTerm('monthly');
        $this->assertEquals('monthly', (string)$interval);
    }

    /** @test */
    public function anInvalidPaymentTermThrowsAnError()
    {
        $this->expectException(InvalidPaymentTermTypeException::class);
        new PaymentTerm('weekly');
    }
}
