<?php

namespace Tests\BNNVARA\Transaction\Domain\ValueObject;

use BNNVARA\Transaction\Domain\Exception\InvalidNameAffixException;
use BNNVARA\Transaction\Domain\ValueObject\Event\NameAffix;
use PHPUnit\Framework\TestCase;

class NameAffixTest extends TestCase {
    /** @test */
    public function aValidNameAffixIsCreated()
    {
        $nameAffix = new NameAffix('van der');
        $this->assertEquals('van der', (string) $nameAffix);
    }

    /** @test */
    public function anInvalidNameAffixThrowsAnError()
    {
        $this->expectException(InvalidNameAffixException::class);
        new NameAffix('sjaakie@dechocoladefabriek.nl');
    }

    /** @test */
    public function aTooLongNameAffixThrowsAnError()
    {
        $this->expectException(InvalidNameAffixException::class);
        new NameAffix('sjaakie en de chocoladefabriek was een suf boek');
    }
}
