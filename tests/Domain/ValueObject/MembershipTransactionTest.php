<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Transaction\Domain\ValueObject;

use BNNVARA\Transaction\Domain\ValueObject\MembershipTransaction;
use PHPUnit\Framework\TestCase;

class MembershipTransactionTest extends TestCase
{
    /** @test */
    public function aMembershipTransactionCanBeCreated(): void
    {
        $membershipTransaction = new MembershipTransaction(
            '91067c81-f45f-450b-8e28-fdbb47f28906',
            '845kls87-df56-347s-749k-j4sje6394ngs8',
            200,
            'paid',
            'test@test.nl'
        );

        $this->assertInstanceOf(
            MembershipTransaction::class,
            $membershipTransaction
        );
        $this->assertSame(
            '91067c81-f45f-450b-8e28-fdbb47f28906',
            $membershipTransaction->getMembershipId()
        );
        $this->assertSame(
            '845kls87-df56-347s-749k-j4sje6394ngs8',
            $membershipTransaction->getTransactionId()
        );
        $this->assertSame(200, $membershipTransaction->getAmountInCents());
        $this->assertSame('paid', $membershipTransaction->getStatus());
        $this->assertSame('test@test.nl', $membershipTransaction->getEmailAddress());
        $this->assertNull($membershipTransaction->getIban());
        $this->assertNull($membershipTransaction->getPaymentTerm());
        $this->assertNull($membershipTransaction->getUtmCollection());
    }

    /**
     * @test
     */
    public function aFullMembershipTransactionCanBeCreated(): void
    {
        $membershipTransaction = new MembershipTransaction(
            '91067c81-f45f-450b-8e28-fdbb47f28906',
            '845kls87-df56-347s-749k-j4sje6394ngs8',
            200,
            'paid',
            'test@test.nl',
            'NL88ABNA1068885432',
            'annual',
            [
                'bnnvara_utm_term' => 'term',
                'bnnvara_utm_source' => 'source',
                'bnnvara_utm_medium' => 'medium',
                'bnnvara_utm_content' => 'content',
                'bnnvara_utm_campaign' => 'campaign',
            ]
        );

        $this->assertInstanceOf(
            MembershipTransaction::class,
            $membershipTransaction
        );
        $this->assertSame(
            '91067c81-f45f-450b-8e28-fdbb47f28906',
            $membershipTransaction->getMembershipId()
        );
        $this->assertSame(
            '845kls87-df56-347s-749k-j4sje6394ngs8',
            $membershipTransaction->getTransactionId()
        );
        $this->assertSame(200, $membershipTransaction->getAmountInCents());
        $this->assertSame('paid', $membershipTransaction->getStatus());
        $this->assertSame('test@test.nl', $membershipTransaction->getEmailAddress());
        $this->assertSame(
            'NL88ABNA1068885432',
            $membershipTransaction->getIban()
        );
        $this->assertSame('annual', $membershipTransaction->getPaymentTerm());
        $this->assertIsArray($membershipTransaction->getUtmCollection());
        $this->assertCount(5, $membershipTransaction->getUtmCollection());
    }
}
