<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Transaction\Domain\Command;

use BNNVARA\Transaction\Domain\Command\TransactionUpsertedEvent;
use BNNVARA\Transaction\Domain\ValueObject\Event\Amount;
use BNNVARA\Transaction\Domain\ValueObject\Event\Email;
use BNNVARA\Transaction\Domain\ValueObject\Event\Iban;
use BNNVARA\Transaction\Domain\ValueObject\Event\Id;
use BNNVARA\Transaction\Domain\ValueObject\Event\PaymentTerm;
use BNNVARA\Transaction\Domain\ValueObject\Event\Status;
use BNNVARA\Transaction\Domain\ValueObject\Event\Transaction;
use PHPUnit\Framework\TestCase;

class TransactionUpsertedEventTest extends TestCase
{
    /** @test */
    public function aTransactionUpsertedEventCanBeCreated()
    {
        $transaction = new Transaction(
            new Id('id'),
            new Amount(500),
            new Status('PAID'),
            new Email('sjaak@example.com'),
            null,
            null,
            null,
            null,
            null,
            null
        );

        $event = new TransactionUpsertedEvent($transaction);

        $this->assertEquals('id', $event->getData()->getId()->getValue());
        $this->assertEquals(500, $event->getData()->getAmount()->getValue());
        $this->assertEquals('PAID', $event->getData()->getStatus()->getValue());
        $this->assertEquals('sjaak@example.com', $event->getData()->getEmail()->getValue());
    }

    /** @test */
    public function aTransactionUpsertedEventCanBeCreatedWithIncasse()
    {
        $transaction = new Transaction(
            new Id('id'),
            new Amount(500),
            new Status('PAID'),
            new Email('sjaak@example.com'),
            null,
            null,
            null,
            new Iban('GB33BUKB20201555555555'),
            new paymentTerm('monthly'),
            null
        );

        $event = new TransactionUpsertedEvent($transaction);

        $this->assertEquals('id', $event->getData()->getId()->getValue());
        $this->assertEquals(500, $event->getData()->getAmount()->getValue());
        $this->assertEquals('PAID', $event->getData()->getStatus()->getValue());
        $this->assertEquals('sjaak@example.com', $event->getData()->getEmail()->getValue());
        $this->assertEquals('GB33BUKB20201555555555', (string)$event->getData()->getIban());
        $this->assertEquals('monthly', (string)$event->getData()->getPaymentTerm());
    }
}
