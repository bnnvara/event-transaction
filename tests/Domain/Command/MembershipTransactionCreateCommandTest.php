<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Transaction\Domain\Command;

use BNNVARA\Transaction\Domain\Command\MembershipTransactionCreateCommand;
use BNNVARA\Transaction\Domain\ValueObject\MembershipTransaction;
use PHPUnit\Framework\TestCase;

class MembershipTransactionCreateCommandTest extends TestCase
{
    /** @test */
    public function aMembershipTransactionCreateCommandCanBeCreated(): void
    {
        $membershipTransaction = $this->getMockBuilder(MembershipTransaction::class)->disableOriginalConstructor(
        )->getMock();

        $membershipPaidCommand = new MembershipTransactionCreateCommand($membershipTransaction);

        $this->assertInstanceOf(
            MembershipTransactionCreateCommand::class,
            $membershipPaidCommand
        );
        $this->assertInstanceOf(
            MembershipTransaction::class,
            $membershipPaidCommand->getData()
        );
    }
}
