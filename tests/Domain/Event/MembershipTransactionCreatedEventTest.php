<?php

declare(strict_types=1);

namespace Tests\BNNVARA\Transaction\Domain\Event;

use BNNVARA\Transaction\Domain\Event\MembershipTransactionCreatedEvent;
use BNNVARA\Transaction\Domain\ValueObject\MembershipTransaction;
use PHPUnit\Framework\TestCase;

class MembershipTransactionCreatedEventTest extends TestCase
{
    /** @test */
    public function aMembershipTransactionCreatedEventCanBeCreated(): void
    {
        $membershipTransaction = new MembershipTransaction(
            '91067c81-f45f-450b-8e28-fdbb47f28906',
            '845kls87-df56-347s-749k-j4sje6394ngs8',
            200,
            'paid',
            'test@test.nl'
        );

        $membershipTransactionCreatedEvent = new MembershipTransactionCreatedEvent($membershipTransaction);

        $this->assertInstanceOf(MembershipTransactionCreatedEvent::class, $membershipTransactionCreatedEvent);
        $this->assertInstanceOf(MembershipTransaction::class, $membershipTransactionCreatedEvent->getData());
    }

}
