<?php

namespace Tests\BNNVARA\Transaction\Domain\Event;

use BNNVARA\Transaction\Domain\Event\ReminderTransactionPaidEvent;
use BNNVARA\Transaction\Domain\ValueObject\ReminderTransaction;
use PHPUnit\Framework\TestCase;

class ReminderTransactionPaidEventTest extends TestCase
{
    /** @test */
    public function aReminderTransactionPaidEventCanBeCreated(): void
    {
        $valueObject = $this->createMock(ReminderTransaction::class);

        $event = new ReminderTransactionPaidEvent($valueObject);

        $this->assertInstanceOf(ReminderTransaction::class, $event->getData());
    }
}
