<?php

declare(strict_types=1);

namespace BNNVARA\Transaction\Domain\Event;

use BNNVARA\Transaction\Domain\ValueObject\MembershipTransaction;

class MembershipTransactionCreatedEvent
{
    private MembershipTransaction $data;

    public function __construct(MembershipTransaction $data)
    {
        $this->data = $data;
    }

    public function getData(): MembershipTransaction
    {
        return $this->data;
    }
}
