<?php

namespace BNNVARA\Transaction\Domain\Event;

use BNNVARA\Transaction\Domain\ValueObject\ReminderTransaction;

class ReminderTransactionPaidEvent
{
    private ReminderTransaction $data;

    public function __construct(ReminderTransaction $data)
    {
        $this->data = $data;
    }

    public function getData(): ReminderTransaction
    {
        return $this->data;
    }
}
