<?php

namespace BNNVARA\Transaction\Domain\Exception;

use \Exception;

class InvalidUrlException extends Exception
{
}