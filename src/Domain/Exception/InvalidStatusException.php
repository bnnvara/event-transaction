<?php

namespace BNNVARA\Transaction\Domain\Exception;

use \Exception;

class InvalidStatusException extends Exception
{
}