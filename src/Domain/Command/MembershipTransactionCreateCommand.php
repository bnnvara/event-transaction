<?php

declare(strict_types=1);

namespace BNNVARA\Transaction\Domain\Command;

use BNNVARA\Transaction\Domain\ValueObject\MembershipTransaction;

class MembershipTransactionCreateCommand implements TransactionCreateCommandInterface
{
    private MembershipTransaction $membershipTransaction;

    public function __construct(MembershipTransaction $membershipTransaction)
    {
        $this->membershipTransaction = $membershipTransaction;
    }

    public function getData(): MembershipTransaction
    {
        return $this->membershipTransaction;
    }
}
