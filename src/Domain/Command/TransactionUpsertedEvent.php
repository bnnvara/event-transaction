<?php

declare(strict_types=1);

namespace BNNVARA\Transaction\Domain\Command;

use BNNVARA\Transaction\Domain\ValueObject\Event\Transaction;

// TODO : this is a Command (specific for a DonationTransaction), class should be renamed
class TransactionUpsertedEvent implements TransactionCreateCommandInterface
{
    private $transaction;

    public function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }

    public function getData(): Transaction
    {
        return $this->transaction;
    }
}
