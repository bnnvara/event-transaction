<?php

namespace BNNVARA\Transaction\Domain\ValueObject\Event;

use BNNVARA\Transaction\Domain\Exception\InvalidLastNameException;

class LastName
{
    const MAX_NAME_LENGTH = 100;

    private string $lastName;

    public function __construct(string $lastName)
    {
        if(strlen($lastName) < self::MAX_NAME_LENGTH && !preg_match('/([0-9_!¡?÷?¿\/\\+=@#$%ˆ&*(){}|~<>;:[\]])/', $lastName))
        {
            $this->lastName = $lastName;
        } else {
            throw new InvalidLastNameException('Lastname value invalid.');
        }
    }

    public function getValue(): string
    {
        return $this->lastName;
    }

    public function __toString(): string
    {
        return $this->getValue();
    }

}
