<?php

namespace BNNVARA\Transaction\Domain\ValueObject\Event;

use BNNVARA\Transaction\Domain\Exception\InvalidNameAffixException;

class NameAffix
{
    const MAX_NAMEAFFIX_LENGTH = 25;

    private string $nameAffix = '';

    public function __construct(string $nameAffix)
    {
        if(strlen($nameAffix) < self::MAX_NAMEAFFIX_LENGTH && !preg_match('/([0-9_!¡?÷?¿\/\\+=@#$%ˆ&*(){}|~<>;:[\]])/', $nameAffix))
        {
            $this->nameAffix = $nameAffix;
        } else {
            throw new InvalidNameAffixException('NameAffix value invalid.');
        }
    }

    public function getValue(): string
    {
        return $this->nameAffix;
    }

    public function __toString(): string
    {
        return $this->getValue();
    }

}
