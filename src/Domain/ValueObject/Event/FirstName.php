<?php

namespace BNNVARA\Transaction\Domain\ValueObject\Event;

use BNNVARA\Transaction\Domain\Exception\InvalidFirstNameException;

class FirstName
{
    const MAX_NAME_LENGTH = 100;

    private string $firstName;

    public function __construct(string $firstName)
    {
        if(strlen($firstName) < self::MAX_NAME_LENGTH && !preg_match('/([0-9_!¡?÷?¿\/\\+=@#$%ˆ&*(){}|~<>;:[\]])/', $firstName))
        {
            $this->firstName = $firstName;
        } else {
            throw new InvalidFirstNameException('First value invalid.');
        }
    }

    public function getValue(): string
    {
        return $this->firstName;
    }

    public function __toString(): string
    {
        return $this->getValue();
    }

}
