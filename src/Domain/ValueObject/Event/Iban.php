<?php

namespace BNNVARA\Transaction\Domain\ValueObject\Event;

class Iban
{
    /** @todo refactor Iban class to AccountNumber, since there are more possibilities for this then IBAN only. */
    private string $iban;

    public function __construct(string $iban)
    {
        $this->iban = $iban;
    }

    public function getValue(): string
    {
        return $this->iban;
    }

    public function __toString()
    {
        return $this->getValue();
    }
}
