<?php

namespace BNNVARA\Transaction\Domain\ValueObject\Event;

class Amount
{
    private int $amount;

    public function __construct(int $amount)
    {
        $this->amount = $amount;
    }

    public function getValue(): int
    {
        return $this->amount;
    }
}
