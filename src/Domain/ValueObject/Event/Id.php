<?php

namespace BNNVARA\Transaction\Domain\ValueObject\Event;

class Id
{
    private string $transactionId;

    public function __construct(string $transactionId)
    {
        $this->transactionId = $transactionId;
    }

    public function getValue(): string
    {
        return $this->transactionId;
    }

    public function __toString(): string
    {
        return $this->getValue();
    }
}
