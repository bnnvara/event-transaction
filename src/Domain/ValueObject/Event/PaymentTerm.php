<?php

namespace BNNVARA\Transaction\Domain\ValueObject\Event;

use BNNVARA\Transaction\Domain\Exception\InvalidPaymentTermTypeException;

class PaymentTerm
{
    const ONCE = 'oneTime';
    const MONTHLY = 'monthly';
    const YEARLY = 'annual';

    private string $paymentTerm;

    /** @throws InvalidPaymentTermTypeException */
    public function __construct(string $paymentTerm)
    {
        $allowedTerms = [
            self::ONCE,
            self::MONTHLY,
            self::YEARLY
        ];

        if (in_array($paymentTerm, $allowedTerms)) {
            $this->paymentTerm = $paymentTerm;
        } else {
            throw new InvalidPaymentTermTypeException();
        }
    }

    public function getValue(): string
    {
        return $this->paymentTerm;
    }

    public function __toString()
    {
        return $this->getValue();
    }
}
