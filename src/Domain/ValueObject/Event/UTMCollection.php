<?php

declare(strict_types=1);

namespace BNNVARA\Transaction\Domain\ValueObject\Event;

class UTMCollection
{
    private array $utmCollection = [];

    public function addUtm(UTM $utm): void
    {
        $this->utmCollection[] = $utm;
    }

    public function getUtmCollection(): array
    {
        return $this->utmCollection;
    }
}