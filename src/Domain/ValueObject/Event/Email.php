<?php

namespace BNNVARA\Transaction\Domain\ValueObject\Event;

use BNNVARA\Transaction\Domain\Exception\InvalidEmailException;

class Email
{
    private string $email;

    public function __construct(string $email)
    {
        if($this->isValidEmail($email) !== FALSE){
            $this->email = $email;
        } else {
            throw new InvalidEmailException();
        }
    }

    public function getValue(): string
    {
        return $this->email;
    }

    public function __toString(): string
    {
        return $this->getValue();
    }

    private function isValidEmail(string $email){
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }
}
