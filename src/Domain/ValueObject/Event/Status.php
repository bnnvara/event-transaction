<?php

namespace BNNVARA\Transaction\Domain\ValueObject\Event;

use BNNVARA\Transaction\Domain\Exception\InvalidStatusException;

class Status
{
    private const STATUS_PAID = 'PAID';
    private const VALID_STATUSSES = [self::STATUS_PAID];

    private string $status;

    public function __construct(string $status)
    {
        if ($this->validateStatus($status)) {
            $this->status = $status;
        } else {
            throw new InvalidStatusException();
        }
    }

    public function getValue(): string
    {
        return $this->status;
    }

    public function __toString(): string
    {
        return $this->getValue();
    }

    private function validateStatus(string $status): bool
    {
        return in_array($status, self::VALID_STATUSSES, false);
    }
}
