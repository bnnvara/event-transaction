<?php

namespace BNNVARA\Transaction\Domain\ValueObject\Event;

class Transaction
{
    private Id $id;
    private Amount $amount;
    private Status $status;
    private Email $email;
    private ?FirstName $firstName = null;
    private ?LastName $lastName = null;
    private ?NameAffix $nameAffix = null;
    private ?Iban $iban = null;
    private ?PaymentTerm $paymentTerm = null;
    private ?UTMCollection $utmCollection = null;

    public function __construct(
        Id $id,
        Amount $amount,
        Status $status,
        Email $email,
        ?FirstName $firstName = null,
        ?LastName $lastName = null,
        ?NameAffix $nameAffix = null,
        ?Iban $iban = null,
        ?PaymentTerm $paymentTerm = null,
        ?UTMCollection $utmCollection = null
    ) {
        $this->id = $id;
        $this->amount = $amount;
        $this->status = $status;
        $this->email = $email;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->nameAffix = $nameAffix;
        $this->iban = $iban;
        $this->paymentTerm = $paymentTerm;
        $this->utmCollection = $utmCollection;
    }

    public function getId(): Id
    {
        return $this->id;
    }

    public function getAmount(): Amount
    {
        return $this->amount;
    }

    public function getStatus(): Status
    {
        return $this->status;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    public function getFirstName(): ?FirstName
    {
        return $this->firstName;
    }

    public function getLastName(): ?LastName
    {
        return $this->lastName;
    }

    public function getNameAffix(): ?NameAffix
    {
        return $this->nameAffix;
    }

    public function getIban(): ?Iban
    {
        return $this->iban;
    }

    public function getPaymentTerm(): ?PaymentTerm
    {
        return $this->paymentTerm;
    }

    public function getUtmCollection(): ?UTMCollection
    {
        return $this->utmCollection;
    }
}
