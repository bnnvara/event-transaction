<?php

declare(strict_types=1);

namespace BNNVARA\Transaction\Domain\ValueObject;

use DateTimeInterface;

class ReminderTransaction
{
    private string $paymentId;
    private string $paymentExternalId;
    private float $amount;
    private DateTimeInterface $paymentDate;

    public function __construct(
        string $paymentId,
        string $paymentExternalId,
        float $amount,
        DateTimeInterface $paymentDate
    ) {
        $this->paymentId = $paymentId;
        $this->paymentExternalId = $paymentExternalId;
        $this->amount = $amount;
        $this->paymentDate = $paymentDate;
    }

    public function getPaymentId(): string
    {
        return $this->paymentId;
    }

    public function getPaymentExternalId(): string
    {
        return $this->paymentExternalId;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function getPaymentDate(): DateTimeInterface
    {
        return $this->paymentDate;
    }
}
