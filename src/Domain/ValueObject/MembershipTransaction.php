<?php

declare(strict_types=1);

namespace BNNVARA\Transaction\Domain\ValueObject;

class MembershipTransaction
{
    private string $membershipId;
    private string $transactionId;
    private int $amountInCents;
    private string $status;
    private string $emailAddress;
    private ?string $iban = null;
    private ?string $paymentTerm = null;
    private ?array $utmCollection = null;

    public function __construct(
        string $membershipId,
        string $transactionId,
        int $amountInCents,
        string $status,
        string $emailAddress,
        ?string $iban = null,
        ?string $paymentTerm = null,
        ?array $utmCollection = null
    ) {
        $this->membershipId = $membershipId;
        $this->transactionId = $transactionId;
        $this->amountInCents = $amountInCents;
        $this->status = $status;
        $this->emailAddress = $emailAddress;
        $this->iban = $iban;
        $this->paymentTerm = $paymentTerm;
        $this->utmCollection = $utmCollection;
    }

    public function getMembershipId(): string
    {
        return $this->membershipId;
    }

    public function getTransactionId(): string
    {
        return $this->transactionId;
    }

    public function getAmountInCents(): int
    {
        return $this->amountInCents;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getEmailAddress(): string
    {
        return $this->emailAddress;
    }

    public function getIban(): ?string
    {
        return $this->iban;
    }

    public function getPaymentTerm(): ?string
    {
        return $this->paymentTerm;
    }

    public function getUtmCollection(): ?array
    {
        return $this->utmCollection;
    }
}
