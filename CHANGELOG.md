# Changelog

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.8.2] - 2021-07-30
- made PHP 8.0 compatible

## [0.8.1] - 2021-03-17
### added
- initializing of optional properties in MembershipTransaction

## [0.8] - 2021-03-15
### added
- extra required fields to MembershipTransactionCreatedEvent

### changed
- renamed MembershipTransactionUpdateCommand to MembershipTransactionCreateCommand
- renamed MembershipTransactionPaidEvent to MembershipTransactionCreatedEvent

## [0.7] - 2021-03-10
### added
- added interfaces for Commands
- added .editorconfig

### changed
- moved TransactionUpsertedEvent to Command folder
- made some properties optional in MembershipTransaction 
- moved docker-compose.yml to root folder
- improvements to Dockerfile

## [0.6] - 2021-03-07
### added
- added MembershipTransactionUpdateCommand

## [0.5] - 2020-03-05
### added
- todo to refactor IBAN to AccountNumber

### removed
- Iban validation, since iban is an AccountNumber, and not perse an Iban

## [0.4.1] - 2020-03-05
 - fixed allowedTerms exposure.
  
## [0.4.1] - 2020-03-03
 - Changed IBAN case to Iban for deserilization

## [0.4] - 2020-02-19
 - Added bank acc number
 - added payment interval
 
## [0.3.2] - 2020-02-14
 - fixed affix name inconsistency for deserialization
 
## [0.3.1] - 2020-02-04
 - fixed "Typed property must not be accessed before initialization"
 
## [0.3] - 2020-02-03
 - added name support
 
## [0.2] - 2020-01-09
 - added support for Email
 - made PHP 7.4 compatible

## [0.1] - 2019-12-19
 - added Event
    - added Amount
    - added Status
    - added Id
